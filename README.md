# MaterialNavigationDrawer

#### 项目介绍
- 项目名称：MaterialNavigationDrawer
- 所属系列：openharmony的第三方组件适配移植
- 功能：具有材料设计风格和简化方法的导航抽屉活动！
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release master

#### 效果演示
![materialnavigationdrawer_harmony](/screenshots/MaterialNavigationDrawer.gif)

#### 安装教程
在moudle级别下的build.gradle文件中添加依赖

 ```groovy
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}

// 添加依赖库
dependencies {
     implementation 'com.gitee.chinasoft_ohos:MaterialNavigationDrawer:1.0.1'
}
 ```

在sdk5，DevEco Studio2.1 beta4下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
* Java 调用示例
###
```
   //继承MaterialAccountListener类,重写初始化init方法
    public void init(IntentParams savedInstanceState) {
   
           // add accounts
           MaterialAccount account = new MaterialAccount(this.getResourceManager(),"NeoKree","neokree@gmail.com", ResourceTable.Media_photo, ResourceTable.Media_bamboo);
           this.addAccount(account);
   
           MaterialAccount account2 = new MaterialAccount(this.getResourceManager(),"Hatsune Miky","hatsune.miku@example.com",ResourceTable.Media_photo2,ResourceTable.Media_mat2);
           this.addAccount(account2);
   
           MaterialAccount account3 = new MaterialAccount(this.getResourceManager(),"Example","example@example.com",ResourceTable.Media_photo,ResourceTable.Media_mat3);
           this.addAccount(account3);
   
           this.setAccountListener(this);
   
           // create sections
           this.addSection(newSection("Section 1", new FragmentIndex()));
           this.addSection(newSection("Section 2",new FragmentIndex()));
           this.addSection(newSection("Section 3",ResourceTable.Media_ic_mic_white_24dp,new FragmentButton()).setSectionColor(Color.getIntColor("#9c27b0")));
           this.addSection(newSection("Section",ResourceTable.Media_ic_hotel_grey600_24dp,new FragmentButton()).setSectionColor(Color.getIntColor("#03a9f4")));
   
           // 此处添加点击按钮后的事件处理逻辑
           Intent secondIntent = new Intent();
           // 指定待启动FA的bundleName和abilityName
           Operation operation = new Intent.OperationBuilder()
                   .withDeviceId("")
                   .withBundleName("it.neokree.materialnavigationdrawer")
                   .withAbilityName("it.neokree.example.Settings")
                   .build();
           secondIntent.setOperation(operation);
   
           // create bottom section
           this.addBottomSection(newSection("Bottom Section",ResourceTable.Media_ic_settings_black_24dp, secondIntent));
       }
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.1


