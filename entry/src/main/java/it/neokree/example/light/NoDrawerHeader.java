
package it.neokree.example.light;

import it.neokree.example.ResourceTable;
import it.neokree.example.mockedFragments.FragmentButton;
import it.neokree.example.mockedFragments.FragmentIndex;
import it.neokree.materialnavigationdrawermodule.MaterialNavigationDrawer;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;
import ohos.agp.utils.Color;

/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public class NoDrawerHeader extends MaterialNavigationDrawer {
    @Override
    public void init(IntentParams savedInstanceState) {
        // create sections
        this.addSection(newSection("Section 1", new FragmentIndex()));
        this.addSection(newSection("Section 2", new FragmentIndex()));
        this.addSection(newSection("Section 3", ResourceTable.Media_ic_mic_white_24dp,
                new FragmentButton()).setSectionColor(Color.getIntColor("#9c27b0")));
        this.addSection(newSection("Section", ResourceTable.Media_ic_hotel_grey600_24dp,
                new FragmentButton()).setSectionColor(Color.getIntColor("#03a9f4")));

        // 此处添加点击按钮后的事件处理逻辑
        Intent secondIntent = new Intent();

        // 指定待启动FA的bundleName和abilityName
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("it.neokree.materialnavigationdrawer")
                .withAbilityName("it.neokree.example.Settings")
                .build();
        secondIntent.setOperation(operation);

        // create bottom section
        this.addBottomSection(newSection("Bottom Section", ResourceTable.Media_ic_settings_black_24dp, secondIntent));
    }
}
