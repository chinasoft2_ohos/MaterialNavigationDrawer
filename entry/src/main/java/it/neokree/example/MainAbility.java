/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.neokree.example;

import it.neokree.materialnavigationdrawermodule.constant.Constant;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;

import java.util.ArrayList;

/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public class MainAbility extends Ability implements ListContainer.ItemClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ListContainer idRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_recycler_view);
        ArrayList<String> list = new ArrayList<>();
        list.add("Light - Mocked Account");
        list.add("Dark  - Mocked Account");
        list.add("Light - Accounts");
        list.add("Dark  - Accounts");
        list.add("Light - Drawer Header Image");
        list.add("Dark  - Drawer Header Image");
        list.add("Light - No Drawer Header");
        list.add("Dark  - No Drawer Header");
        list.add("Light - Custom Drawer Header");
        list.add("Dark  - Custom Drawer Header");
        list.add("Functionality: unique Toolbar Color");
        list.add("Functionality: ripple backport support");
        list.add("Functionality: multi pane support for tablet");
        list.add("Functionality: custom section under account list");
        list.add("Functionality: Kitkat trasluncent status bar");
        list.add("Functionality: Master/Child example");
        list.add("Functionality: section not pre-rendered");
        list.add("Functionality: default section loaded");
        list.add("Functionality: action bar shadow enabled (toolbar elevation)");
        list.add("Functionality: learning pattern disabled");
        list.add("Back Pattern: Back To first");
        list.add("Back Pattern: Back Anywhere");
        list.add("Back Pattern: Custom");
        idRecyclerView.setItemProvider(new BaseItemProvider() {
            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
                Component cpt;
                if (convertComponent == null) {
                    cpt = LayoutScatter.getInstance(componentContainer.getContext()).parse(
                        ResourceTable.Layout_simple_list_item_1, componentContainer, false);
                } else {
                    cpt = convertComponent;
                }

                Text idText1 = (Text) cpt.findComponentById(ResourceTable.Id_text1);
                idText1.setText(list.get(i));

                return cpt;
            }
        });
        idRecyclerView.setLongClickable(false);
        idRecyclerView.setItemClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
        Operation operation;
        Intent intent = new Intent();
        IntentParams intentParams = new IntentParams();
        switch (position) {
            case 0:
            case 1:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.light.MockedAccount")
                    .build();
                break;
            case 2:
            case 3:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.light.Accounts")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 4:
            case 5:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.light.ImageDrawerHeader")
                    .build();
                break;
            case 6:
            case 7:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.light.NoDrawerHeader")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 8:
            case 9:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.light.CustomDrawerHeader")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 2);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 10:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.UniqueToolbarColor")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intentParams.setParam(Constant.MaterialNavigationDrawer.UNIQUE_TOOLBAR_COLOR, true);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 11:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.RippleBackport")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 12:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.MultiPane")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 13:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.CustomAccountSection")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 14:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.KitkatStatusBar")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 15:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.master_child.MasterChildActivity")
                    .build();
                break;
            case 16:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.RealColorSections")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 17:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.DefaultSectionLoaded")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intentParams.setParam(Constant.MaterialNavigationDrawer.DEFAULT_SECTION_LOADED, 3);
                intentParams.setParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 18:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.ActionBarShadow")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 19:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.functionalities.LearningPatternDisabled")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intentParams.setParam(Constant.MaterialNavigationDrawer.LEARNING_PATTERN, false);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 20:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.backpattern.BackToFirst")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            case 21:
                operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("it.neokree.materialnavigationdrawer")
                    .withAbilityName("it.neokree.example.backpattern.BackAnywhere")
                    .build();
                intentParams.setParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE, 3);
                intent.setParam(Constant.MaterialNavigationDrawer.KEY, intentParams);
                break;
            default:
                operation = null;
        }
        if (operation != null) {
            intent.setOperation(operation);
            startAbility(intent);
        }
    }
}
