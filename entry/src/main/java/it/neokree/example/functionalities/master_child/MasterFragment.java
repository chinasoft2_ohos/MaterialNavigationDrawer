
package it.neokree.example.functionalities.master_child;

import it.neokree.example.ResourceTable;
import it.neokree.materialnavigationdrawermodule.MaterialNavigationDrawer;
import it.neokree.materialnavigationdrawermodule.view.Toolbar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * lipeiquan
 *
 * @since 2021-05-11
 */
public class MasterFragment extends Fraction implements Component.ClickedListener {

    private Component component;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = scatter.parse(ResourceTable.Layout_masterchild_master, container, false);
        Button button = (Button) component.findComponentById(ResourceTable.Id_master_button);
        button.setText("OPEN CHILD");
        button.setClickedListener(this::onClick);
        ((MaterialNavigationDrawer) this.getFractionAbility()).changeToolbarIcon(Toolbar.IconType.iconMenu);
        return component;
    }

    @Override
    public void onClick(Component c) {
        ((MaterialNavigationDrawer) this.getFractionAbility()).setFragmentChild(new ChildFragment(), "Child Title");
    }

}
