
package it.neokree.example.functionalities.master_child;

import it.neokree.example.ResourceTable;
import it.neokree.materialnavigationdrawermodule.MaterialNavigationDrawer;
import it.neokree.materialnavigationdrawermodule.view.Toolbar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * lipeiquan
 *
 * @since 2021-05-11
 */
public class ChildFragment extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        ((MaterialNavigationDrawer) this.getFractionAbility()).changeToolbarIcon(Toolbar.IconType.iconBack);
        return scatter.parse(ResourceTable.Layout_masterchild_child, container, false);
    }
}
