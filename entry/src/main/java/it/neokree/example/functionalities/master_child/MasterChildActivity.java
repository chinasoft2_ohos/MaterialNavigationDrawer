
package it.neokree.example.functionalities.master_child;

import it.neokree.example.ResourceTable;
import it.neokree.materialnavigationdrawermodule.MaterialNavigationDrawer;
import it.neokree.materialnavigationdrawermodule.util.Utils;
import ohos.aafwk.content.IntentParams;

/**
 * lipeiquan
 *
 * @since 2021-05-11
 */
public class MasterChildActivity extends MaterialNavigationDrawer {
    @Override
    public void init(IntentParams savedInstanceState) {
        this.setDrawerHeaderImage(Utils.getPixelMap(this, ResourceTable.Media_mat3));

        this.addSection(this.newSection("Section 1", new MasterFragment()));
        this.addSection(this.newSection("Section 2", new MasterFragment()));
    }
}
