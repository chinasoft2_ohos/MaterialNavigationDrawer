
package it.neokree.example.mockedFragments;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;


/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public class FragmentIndex extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        DependentLayout.LayoutConfig layoutConfig = new DependentLayout.LayoutConfig(
            DependentLayout.LayoutConfig.MATCH_PARENT, DependentLayout.LayoutConfig.MATCH_PARENT);
        layoutConfig.addRule(DependentLayout.LayoutConfig.CENTER_IN_PARENT);
        Text text = new Text(this.getFractionAbility());
        text.setText("Section");
        text.setTextAlignment(TextAlignment.CENTER);
        text.setTextSize(14, Text.TextSizeType.FP);
        text.setTextColor(new Color(Color.getIntColor("#B3B3B3")));
        text.setTextAlignment(TextAlignment.CENTER);
        text.setLayoutConfig(layoutConfig);
        return text;
    }

}
