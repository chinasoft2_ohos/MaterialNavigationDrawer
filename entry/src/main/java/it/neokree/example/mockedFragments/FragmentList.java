
package it.neokree.example.mockedFragments;

import it.neokree.example.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;

/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public class FragmentList extends Fraction {
    ArrayList<String> list;
    private static final int mConut = 50;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        list = new ArrayList<>();

        for (int i = 0; i < mConut; i++) {
            list.add("Item " + (i + 1));
        }
        Component component = scatter.parse(ResourceTable.Layout_ability_main, container, false);
        ListContainer idRecyclerView = (ListContainer) component.findComponentById(ResourceTable.Id_recycler_view);

        idRecyclerView.setItemProvider(new BaseItemProvider() {
            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
                Component cpt;
                if (convertComponent == null) {
                    cpt = LayoutScatter.getInstance(componentContainer.getContext()).parse(
                            ResourceTable.Layout_simple_list_item_1, componentContainer, false);
                } else {
                    cpt = convertComponent;
                }

                Text idText = (Text) cpt.findComponentById(ResourceTable.Id_text1);
                idText.setText(list.get(i));

                return cpt;
            }
        });
        return component;
    }
}
