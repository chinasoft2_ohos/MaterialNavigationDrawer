
package it.neokree.example.mockedFragments;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;

/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public class FragmentButton extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
        directionalLayout.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        directionalLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_PARENT);
        directionalLayout.setOrientation(Component.VERTICAL);
        directionalLayout.setPadding(15, 15, 15, 15);

        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                DependentLayout.LayoutConfig.MATCH_PARENT, DependentLayout.LayoutConfig.MATCH_PARENT);
        layoutConfig.alignment = LayoutAlignment.CENTER;

        Button button = new Button(this.getFractionAbility());
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(214, 214, 214));
        background.setCornerRadius(25);
        button.setBackground(background);
        button.setText("CLICK ME");
        button.setTextSize(15, Text.TextSizeType.FP);
        button.setTextAlignment(TextAlignment.CENTER);
        button.setLayoutConfig(layoutConfig);
        directionalLayout.addComponent(button);
        return directionalLayout;
    }

}
