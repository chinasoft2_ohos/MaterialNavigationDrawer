
package it.neokree.materialnavigationdrawermodule.util;


import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.SlideDrawer;

import it.neokree.materialnavigationdrawermodule.callback.DrawerListener;
import it.neokree.materialnavigationdrawermodule.elements.MaterialSection;
import it.neokree.materialnavigationdrawermodule.view.Toolbar;

/**
 * Created by neokree on 14/02/15.
 */
public abstract class MaterialActionBarDrawerToggle<Fraction> implements DrawerListener {

    private MaterialSection<Fraction> requestedSection;
    private boolean isCurrentFragmentChild;
    private boolean request;
    private Component.ClickedListener mToolbarNavigationClickListener;

    public MaterialActionBarDrawerToggle(Ability activity, SlideDrawer drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
        drawerLayout.addSlideListener(new SlideDrawer.SlideListener() {
            @Override
            public void onOpen(SlideDrawer slideDrawer, SlideDrawer.SlideDirection slideDirection) {
                onDrawerOpened(slideDrawer);
            }

            @Override
            public void onMiddle(SlideDrawer slideDrawer, SlideDrawer.SlideDirection slideDirection, SlideDrawer.DrawerState drawerState) {

            }

            @Override
            public void onClose(SlideDrawer slideDrawer, SlideDrawer.SlideDirection slideDirection) {
                onDrawerClosed(slideDrawer);
            }

            @Override
            public void onSlideChange(SlideDrawer slideDrawer, SlideDrawer.SlideDirection slideDirection, int i, int i1) {
                onDrawerSlide(slideDrawer, i);
            }
        });
        toolbar.setToolbarListener(new Toolbar.ToolbarListener() {
            @Override
            public void onClickIcon() {
                if (isCurrentFragmentChild) {
                    mToolbarNavigationClickListener.onClick(null);
                } else {
                    drawerLayout.openSmoothly();
                }
            }
        });
        request = false;
    }

    public void setToolbarNavigationClickListener(
        Component.ClickedListener onToolbarNavigationClickListener) {
        mToolbarNavigationClickListener = onToolbarNavigationClickListener;
    }

    public void addRequest(MaterialSection section) {
        request = true;
        requestedSection = section;
    }

    public void removeRequest() {
        request = false;
        requestedSection = null;
    }

    public boolean hasRequest() {
        return request;
    }

    public MaterialSection getRequestedSection() {
        return requestedSection;
    }

    public void setCurrentFragmentChild(boolean isCurrentFragmentChild) {
        this.isCurrentFragmentChild = isCurrentFragmentChild;
    }

}
