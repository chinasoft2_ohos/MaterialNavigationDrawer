
package it.neokree.materialnavigationdrawermodule.util;

import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageInfo;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.InputStream;
import java.util.Locale;

/**
 * Class containing some static utility methods.
 * <p>
 * Created by neokree on 06/01/15.
 */
public class Utils {
    private static final int STATUS_BAR_HEIGHT_DEFAULT = 129;
    private Utils() {
    }

    public static int getDrawerWidth(ResourceManager res) {

        int i = ((res.getDeviceCapability().width * 3) - (56 * (res.getDeviceCapability().screenDensity / 160)));
        return i;
    }

    public static boolean isTablet(ResourceManager res) {
        return true;
    }

    public static int getScreenHeight(AbilityContext act) {
        int density = act.getResourceManager().getDeviceCapability().screenDensity / 160;
        int height1 = act.getResourceManager().getDeviceCapability().height;
        return density * height1;
    }

    public static Point getUserPhotoSize(ResourceManager res) {
        int size = (int) (64 * (res.getDeviceCapability().screenDensity / 160));

        return new Point(size, size);
    }

    public static Point getBackgroundSize(ResourceManager res) {
        int width = getDrawerWidth(res);

        int height = (9 * width) / 16;

        return new Point(width, height);
    }

    public static PixelMap resizeBitmapFromResource(ResourceManager res, int resId, int reqWidth, int reqHeight) {

        try (InputStream inputStream = res.getResource(resId)) {
            ImageSource imageSource = ImageSource.create(inputStream, null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.desiredSize = new Size(reqWidth, reqHeight);
            decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
            return imageSource.createPixelmap(decodingOpts);
        } catch (Exception e) {
            return null;
        }
    }

    public static PixelMap resizeBitmap(PixelMap bitmap, int reqWidth, int reqHeight) {
        ImageInfo imageInfo = bitmap.getImageInfo();
        imageInfo.size.height = reqHeight;
        imageInfo.size.width = reqWidth;
        return bitmap;

    }

    public static boolean isRTL() {
        Locale defLocale = Locale.getDefault();
        final int directionality = Character.getDirectionality(defLocale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
            directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }

    public static void setAlpha(Component v, float alpha) {
        v.setAlpha(alpha);
    }

    public static PixelMap getPixelMap(Context context, int resId) {
        try (InputStream inputStream = context.getResourceManager().getResource(resId)) {
            ImageSource imageSource = ImageSource.create(inputStream, null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.desiredSize = new Size(0, 0);
            decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
            return imageSource.createPixelmap(decodingOpts);
        } catch (Exception e) {
            return null;
        }
    }

    public static int getScreenHeight(Component component) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(component.getContext()).get();
        return display.getRealAttributes().height;
    }

    public static int getAppHeight(Component component){
        Display display = DisplayManager.getInstance().getDefaultDisplay(component.getContext()).get();
        return display.getAttributes().height;
    }

    public static int getStatusBarHeight(Component component){
        ohos.agp.utils.Rect rect = new ohos.agp.utils.Rect();
        component.getWindowVisibleRect(rect);
        int statusBarHeight = rect.top;
        return statusBarHeight;
    }

    public static int getNavBarHeight(Component component) {
        return getScreenHeight(component) - getAppHeight(component) - getStatusBarHeight(component);
    }
}
