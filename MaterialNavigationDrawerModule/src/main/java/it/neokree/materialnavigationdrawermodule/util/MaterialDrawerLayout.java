
package it.neokree.materialnavigationdrawermodule.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.SlideDrawer;
import ohos.app.Context;


public class MaterialDrawerLayout extends SlideDrawer {

    public MaterialDrawerLayout(Context context) {
        this(context, null);
    }

    public MaterialDrawerLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public MaterialDrawerLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public void setMultipaneSupport(boolean support) {
        if (Utils.isTablet(this.getResourceManager())) {
            // custom implementation only for tablets
        }
    }

    public void setDrawerListener(MaterialActionBarDrawerToggle materialActionBarDrawerToggle) {
    }
}
