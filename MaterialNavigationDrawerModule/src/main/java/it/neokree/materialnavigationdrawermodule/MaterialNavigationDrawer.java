
package it.neokree.materialnavigationdrawermodule;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.SlideDrawer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.ResourceManager;
import ohos.media.image.PixelMap;

import java.util.LinkedList;
import java.util.List;

import it.neokree.materialnavigationdrawermodule.callback.DrawerListener;
import it.neokree.materialnavigationdrawermodule.constant.Constant;
import it.neokree.materialnavigationdrawermodule.elements.Element;
import it.neokree.materialnavigationdrawermodule.elements.MaterialAccount;
import it.neokree.materialnavigationdrawermodule.elements.MaterialSection;
import it.neokree.materialnavigationdrawermodule.elements.MaterialSubheader;
import it.neokree.materialnavigationdrawermodule.elements.listeners.MaterialAccountListener;
import it.neokree.materialnavigationdrawermodule.elements.listeners.MaterialSectionListener;
import it.neokree.materialnavigationdrawermodule.util.MaterialActionBarDrawerToggle;
import it.neokree.materialnavigationdrawermodule.util.MaterialDrawerLayout;
import it.neokree.materialnavigationdrawermodule.util.TypefaceManager;
import it.neokree.materialnavigationdrawermodule.util.Utils;
import it.neokree.materialnavigationdrawermodule.view.RoundImage;
import it.neokree.materialnavigationdrawermodule.view.Toolbar;

/**
 * lpq
 *
 * @since 2021-05-06
 */
public abstract class MaterialNavigationDrawer<Fraction> extends FractionAbility implements MaterialSectionListener, MaterialAccount.OnAccountDataLoaded {

    private static final int USER_CHANGE_TRANSITION = 500;

    public static final int BACKPATTERN_BACK_ANYWHERE = 0;
    public static final int BACKPATTERN_BACK_TO_FIRST = 1;
    public static final int BACKPATTERN_CUSTOM = 2;

    private static final int DRAWERHEADER_ACCOUNTS = 0;
    private static final int DRAWERHEADER_IMAGE = 1;
    private static final int DRAWERHEADER_CUSTOM = 2;
    private static final int DRAWERHEADER_NO_HEADER = 3;

    private static final String STATE_SECTION = "MaterialNavigationDrawer_section";
    private static final String STATE_LIST = "MaterialNavigationDrawer_list";
    private static final String STATE_ACCOUNT = "MaterialNavigationDrawer_account";

    private MaterialDrawerLayout drawerLayout;
    private MaterialActionBarDrawerToggle pulsante;
    private Image statusBar;
    private Toolbar toolbar;
    private DependentLayout dependentDrawer;
    private Image userphoto;
    private Image userSecondPhoto;
    private Image userThirdPhoto;
    private Image usercover;
    private Text username;
    private Text usermail;
    private Image userButtonSwitcher;
    private DirectionalLayout customDrawerHeader;
    private DirectionalLayout sections;
    private DirectionalLayout bottomSections;
    private DependentLayout parentContent;

    // Lists
    private List<MaterialSection> sectionList;
    private List<MaterialSection> bottomSectionList;
    private List<MaterialAccount> accountManager;
    private List<MaterialSection> accountSectionList;
    private List<MaterialSubheader> subheaderList;
    private List<Element> elementsList;
    private List<Fraction> childFragmentStack;
    private List<String> childTitleStack;

    // current pointers
    private MaterialSection currentSection;
    private MaterialAccount currentAccount;

    private float density;
    private int primaryColor;
    private int primaryDarkColor;
    private int drawerColor;
    private boolean drawerTouchLocked = false;
    private boolean slidingDrawerEffect = false;
    private boolean rippleSupport;
    private boolean uniqueToolbarColor;
    private boolean singleAccount;
    private boolean accountSwitcher = false;
    private boolean isCurrentFragmentChild = false;
    private boolean learningPattern = true;
    private boolean hasRefreshDependentDrawer = false;
    private int backPattern = BACKPATTERN_BACK_ANYWHERE;
    private int drawerHeaderType;
    private int defaultSectionLoaded = 0;

    // resources
    private ResourceManager resources;
    private TypefaceManager fontManager;


    // listeners
    private Component.ClickedListener currentAccountListener = new Component.ClickedListener() {

        @Override
        public void onClick(Component v) {

            if (!drawerTouchLocked) {
                // enter into account properties
                if (accountListener != null) {
                    accountListener.onAccountOpening(currentAccount);
                }

                if (!deviceSupportMultiPane()) {
                    drawerLayout.closeSmoothly();
                }

            }
        }
    };

    private Component.ClickedListener secondAccountListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            if (!drawerTouchLocked) {

                // account change
                MaterialAccount account = findAccountNumber(MaterialAccount.SECOND_ACCOUNT);
                if (account != null) {
                    if (accountListener != null)
                        accountListener.onChangeAccount(account);

                    switchAccounts(account);
                } else {// if there is no second account user clicked for open it
                    if (accountSwitcherListener != null && !singleAccount)
                        accountSwitcherListener.onClick(null);
                }
            }
        }
    };
    private Component.ClickedListener thirdAccountListener = new Component.ClickedListener() {

        @Override
        public void onClick(Component component) {
            if (!drawerTouchLocked) {

                // account change
                MaterialAccount account = findAccountNumber(MaterialAccount.THIRD_ACCOUNT);
                if (account != null) {
                    if (accountListener != null) {
                        accountListener.onChangeAccount(account);
                    }

                    switchAccounts(account);
                } else {// if there is no second account user clicked for open it
                    if (accountSwitcherListener != null && !singleAccount)
                        accountSwitcherListener.onClick(null);
                }
            }
        }
    };

    private Component.ClickedListener accountSwitcherListener = new Component.ClickedListener() {

        @Override
        public void onClick(Component v) {
            if (!drawerTouchLocked) {

                // si rimuovono le viste  || Views are removed
                sections.removeAllComponents();

                if (!accountSwitcher) {
                    // si cambia l'icona del pulsante || Change the icon of the button
                    userButtonSwitcher.setPixelMap(ResourceTable.Media_ic_arrow_drop_up_white_24dp);
                    bottomSections.setVisibility(Component.HIDE);

                    for (MaterialAccount account : accountManager) {
                        // si inseriscono tutti gli account ma non quello attualmente in uso || Add all account without the current one
                        if (account.getAccountNumber() != MaterialAccount.FIRST_ACCOUNT) {
                            DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (56 * density));
                            sections.addComponent(account.getSectionView(MaterialNavigationDrawer.this, fontManager.getRobotoMedium(MaterialNavigationDrawer.this), accountSectionListener, rippleSupport, account.getAccountNumber()), params);
                        }
                    }
                    for (MaterialSection section : accountSectionList) {
                        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (48 * density));
                        sections.addComponent(section.getView(), params);
                    }

                    // si attiva l'account switcher per annotare che si visualizzano gli account. || accountSwitcher is enabled for checking the account list is showed.
                    accountSwitcher = true;
                } else {
                    userButtonSwitcher.setPixelMap(ResourceTable.Media_ic_arrow_drop_down_white_24dp);
                    bottomSections.setVisibility(Component.VISIBLE);
                    int indexSection = 0, indexSubheader = 0;
                    for (Element element : elementsList) {
                        switch (element.getType()) {
                            case Element.TYPE_SECTION:
                                MaterialSection section = sectionList.get(indexSection);
                                indexSection++;
                                DirectionalLayout.LayoutConfig paramSection = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (48 * density));
                                sections.addComponent(section.getView(), paramSection);
                                break;
                            case Element.TYPE_DIVISOR:
                                Component view = new Component(MaterialNavigationDrawer.this);
                                ShapeElement shapeElement = new ShapeElement();
                                shapeElement.setRgbColor(RgbColor.fromArgbInt(ohos.agp.utils.Color.getIntColor("#8f8f8f")));
                                view.setBackground(shapeElement);
                                // height 1 px
                                DirectionalLayout.LayoutConfig paramDivisor = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, 1);
                                paramDivisor.setMargins(0, (int) (8 * density), 0, (int) (8 * density));
                                sections.addComponent(view, paramDivisor);
                                break;
                            case Element.TYPE_SUBHEADER:
                                MaterialSubheader subheader = subheaderList.get(indexSubheader);
                                indexSubheader++;
                                sections.addComponent(subheader.getView());
                                break;
                            case Element.TYPE_BOTTOM_SECTION:
                                break; // le bottom section vengono gestite dopo l'inserimento degli altri elementi
                        }
                    }
                    addButtonView();
                    accountSwitcher = false;
                }

            }
        }
    };

    private MaterialSectionListener accountSectionListener = new MaterialSectionListener() {
        @Override
        public void onClick(MaterialSection section) {
            section.unSelect(); // remove the selected color

            if (!drawerTouchLocked) {
                int accountPosition = section.getAccountPosition();
                MaterialAccount account = findAccountNumber(accountPosition);

                // switch accounts position
                currentAccount.setAccountNumber(accountPosition);
                account.setAccountNumber(MaterialAccount.FIRST_ACCOUNT);
                currentAccount = account;

                notifyAccountDataChanged();

                // call change account method
                if (accountListener != null)
                    accountListener.onChangeAccount(account);

                // change account list
                accountSwitcherListener.onClick(null);
            }
        }
    };
    private Component id_dl_null;


    @Override
    public void onClick(MaterialSection section) {
        switch (section.getTarget()) {
            case MaterialSection.TARGET_FRAGMENT:
                // se l'utente clicca sulla stessa schermata in cui si trova si chiude il drawer e basta
                if (section == currentSection) {
                    drawerLayout.closeSmoothly();
                    return;
                }
                if (deviceSupportMultiPane()) {
                    changeToolbarColor(section);
                    setFragment((Fraction) section.getTargetFragment(), section.getTitle(), (Fraction) currentSection.getTargetFragment());
                    afterFragmentSetted((Fraction) section.getTargetFragment(), section.getTitle());
                } else {
                    // si disattiva il touch sul drawer
                    setDrawerTouchable(false);
                    // la chiamata al fragment viene spostata dopo la chiusura del drawer
                    pulsante.addRequest(section);
                    getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            drawerLayout.closeSmoothly();
                        }
                    }, 200);
                }
                break;
            case MaterialSection.TARGET_ACTIVITY:
                this.startAbility(section.getTargetIntent());
                if (!deviceSupportMultiPane()) {
                    drawerLayout.closeSmoothly();
                }
                break;
            // TARGET_LISTENER viene gestito internamente
            case MaterialSection.TARGET_LISTENER:
                if (!deviceSupportMultiPane()) {
                    drawerLayout.closeSmoothly();
                }

                if (!deviceSupportMultiPane()) {
                    setDrawerTouchable(false);
                }
            default:
                break;
        }

        // se il target e' un activity la sezione corrente rimane quella precedente
        if (section.getTarget() != MaterialSection.TARGET_ACTIVITY) {
            syncSectionsState(section);
        }
    }

    private void syncSectionsState(MaterialSection section) {
        currentSection = section;

        // search in first list
        int position = sectionList.indexOf(section);
        if (position != -1) {
            for (int i = 0; i < sectionList.size(); i++)
                if (i != position)
                    sectionList.get(i).unSelect();
        } else {
            // section is a bottom section
            position = bottomSectionList.indexOf(section);
            for (int i = 0; i < bottomSectionList.size(); i++)
                if (i != position)
                    bottomSectionList.get(i).unSelect();
        }
    }

    private MaterialAccountListener accountListener;
    private DrawerListener drawerListener;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);

        drawerHeaderType = 0;
        rippleSupport = true;
        uniqueToolbarColor = false;
        singleAccount = true;
        learningPattern = false;
        drawerColor = Color.WHITE.getValue();
        defaultSectionLoaded = 0;

        IntentParams param = intent.getParam(Constant.MaterialNavigationDrawer.KEY);
        if (param != null) {
            if (param.hasParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT)) {
                singleAccount = (boolean) param.getParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT);
            }
            if (param.hasParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE)) {
                drawerHeaderType = (int) param.getParam(Constant.MaterialNavigationDrawer.DRAWER_TYPE);
            }
            if (param.hasParam(Constant.MaterialNavigationDrawer.UNIQUE_TOOLBAR_COLOR)) {
                uniqueToolbarColor = (boolean) param.getParam(Constant.MaterialNavigationDrawer.UNIQUE_TOOLBAR_COLOR);
            }
            if (param.hasParam(Constant.MaterialNavigationDrawer.LEARNING_PATTERN)) {
                learningPattern = (boolean) param.getParam(Constant.MaterialNavigationDrawer.LEARNING_PATTERN);
            }
            if (param.hasParam(Constant.MaterialNavigationDrawer.DEFAULT_SECTION_LOADED)) {
                defaultSectionLoaded = (int) param.getParam(Constant.MaterialNavigationDrawer.DEFAULT_SECTION_LOADED);
            }
            if (param.hasParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT)) {
                singleAccount = (boolean) param.getParam(Constant.MaterialNavigationDrawer.SINGLE_ACCOUNT);
            }
        }

        if (drawerHeaderType == DRAWERHEADER_ACCOUNTS) {
            super.setUIContent(ResourceTable.Layout_activity_material_navigation_drawer);
        } else {
            super.setUIContent(ResourceTable.Layout_activity_drawer_customheader);
        }


        // init Typeface
        fontManager = new TypefaceManager(this);

        // init toolbar & status bar
        statusBar = (Image) findComponentById(ResourceTable.Id_statusBar);
        toolbar = (Toolbar) findComponentById(ResourceTable.Id_toolbar);
        toolbar.setToolbarListener(new Toolbar.ToolbarListener() {
            @Override
            public void onClickIcon() {
                onBackPressed();
            }
        });

        parentContent = (DependentLayout) this.findComponentById(ResourceTable.Id_content);

        parentContent.setLayoutRefreshedListener(new Component.LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                if (hasRefreshDependentDrawer) {
                    return;
                }
                if (Utils.getStatusBarHeight(component) > 0) {
                    int height = Utils.getScreenHeight(component) - Utils.getNavBarHeight(component);
//                    HiLog.info(hiLogLabel, "Utils.getScreenHeight(component) = " + Utils.getScreenHeight(component));
//                    HiLog.info(hiLogLabel, "Utils.getAppHeight(component) = " + Utils.getAppHeight(component));
//                    HiLog.info(hiLogLabel, "Utils.getStatusBarHeight(component) = " + Utils.getStatusBarHeight(component));
//                    HiLog.info(hiLogLabel, "Utils.getNavBarHeight(component) = " + Utils.getNavBarHeight(component));
//                    HiLog.info(hiLogLabel, "height = " + height);
                    initView(intent, height);
                    hasRefreshDependentDrawer = true;
                }
            }
        });
    }

    private void initView(Intent intent, int height) {
        // init drawer components
        drawerLayout = (MaterialDrawerLayout) this.findComponentById(ResourceTable.Id_drawer_layout);
        dependentDrawer = (DependentLayout) this.findComponentById(ResourceTable.Id_drawer);

        id_dl_null = this.findComponentById(ResourceTable.Id_dl_null);
        id_dl_null.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawerLayout.closeSmoothly();
            }
        });
        id_dl_null.setClickable(false);

        SlideDrawer.LayoutConfig layoutConfig = new SlideDrawer.LayoutConfig(500, height, SlideDrawer.SlideDirection.START);
        dependentDrawer.setLayoutConfig(layoutConfig);

        if (drawerHeaderType == DRAWERHEADER_ACCOUNTS) {
            username = (Text) this.findComponentById(ResourceTable.Id_user_nome);
            usermail = (Text) this.findComponentById(ResourceTable.Id_user_email);
            userphoto = (Image) this.findComponentById(ResourceTable.Id_user_photo);

            userSecondPhoto = (Image) this.findComponentById(ResourceTable.Id_user_photo_2);
            userThirdPhoto = (Image) this.findComponentById(ResourceTable.Id_user_photo_3);
            usercover = (Image) this.findComponentById(ResourceTable.Id_user_cover);
            userButtonSwitcher = (Image) this.findComponentById(ResourceTable.Id_user_switcher);

            // set the button image
            if (!singleAccount) {
                userButtonSwitcher.setPixelMap(ResourceTable.Media_ic_arrow_drop_down_white_24dp);
                userButtonSwitcher.setClickedListener(accountSwitcherListener);
            }

        } else {
            customDrawerHeader = (DirectionalLayout) this.findComponentById(ResourceTable.Id_drawer_header);
            if (drawerHeaderType == DRAWERHEADER_NO_HEADER) {
                customDrawerHeader.setVisibility(Component.HIDE);
            }
        }

        sections = (DirectionalLayout) this.findComponentById(ResourceTable.Id_sections);
        bottomSections = (DirectionalLayout) this.findComponentById(ResourceTable.Id_bottom_sections);

        // init lists
        sectionList = new LinkedList<>();
        bottomSectionList = new LinkedList<>();
        accountManager = new LinkedList<>();
        accountSectionList = new LinkedList<>();
        subheaderList = new LinkedList<>();
        elementsList = new LinkedList<>();
        childFragmentStack = new LinkedList<>();
        childTitleStack = new LinkedList<>();
        childTitleStack = new LinkedList<>();

        // init listeners
        if (drawerHeaderType == DRAWERHEADER_ACCOUNTS) {
            userphoto.setClickedListener(currentAccountListener);
            if (singleAccount) {
                usercover.setClickedListener(currentAccountListener);
            } else {
                usercover.setClickedListener(accountSwitcherListener);
            }

            userSecondPhoto.setClickedListener(secondAccountListener);
            userThirdPhoto.setClickedListener(thirdAccountListener);

        }

        // set drawer backgrond color
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(drawerColor));
        dependentDrawer.setBackground(shapeElement);

        //get resources and density
        resources = this.getResourceManager();
        density = (float) resources.getDeviceCapability().screenDensity / (float) 160;

        // set the right drawer width
        ComponentContainer.LayoutConfig drawerParams = dependentDrawer.getLayoutConfig();
        drawerParams.width = Utils.getDrawerWidth(getResourceManager());
        dependentDrawer.setLayoutConfig(drawerParams);

        primaryColor = -7617718;

        primaryDarkColor = Color.getIntColor("#8BC349");

        // DEVELOPER CALL TO INIT
        init(intent.getParams());

        if (sectionList.size() == 0) {
            throw new RuntimeException("You must add at least one Section to top list.");
        }

        if (deviceSupportMultiPane()) {
            SlideDrawer.LayoutConfig params = new SlideDrawer.LayoutConfig(SlideDrawer.LayoutConfig.MATCH_PARENT, SlideDrawer.LayoutConfig.MATCH_PARENT);
            params.setMargins((int) (320 * density), 0, 0, 0);
            drawerLayout.openSmoothly();
            drawerLayout.setMultipaneSupport(true);
        } else {
            pulsante = new MaterialActionBarDrawerToggle<Fraction>(this, drawerLayout, getToolbar(), ResourceTable.String_nothing, ResourceTable.String_nothing) {

                @Override
                public void onDrawerClosed(Component view) {
                    getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            id_dl_null.setClickable(false);
                            // abilita il touch del drawer
                            setDrawerTouchable(true);
                            if (drawerListener != null)
                                drawerListener.onDrawerClosed(view);
                            if (hasRequest()) {
                                MaterialSection section = getRequestedSection();
                                changeToolbarColor(section);
                                setFragment((Fraction) section.getTargetFragment(), section.getTitle(), (Fraction) currentSection.getTargetFragment());
                                afterFragmentSetted((Fraction) section.getTargetFragment(), section.getTitle());
                                pulsante.removeRequest();
                            }
                        }
                    });
                }

                @Override
                public void onDrawerOpened(Component drawerView) {
                    drawerView.getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            id_dl_null.setClickable(true);

                            if (drawerListener != null)
                                drawerListener.onDrawerOpened(drawerView);
                        }
                    });
                }

                @Override
                public void onDrawerSlide(Component drawerView, float slideOffset) {
                    drawerView.getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            // if user wants the sliding arrow it compare
                            if (slidingDrawerEffect) {
                            } else if (drawerListener != null)
                                drawerListener.onDrawerSlide(drawerView, slideOffset);
                        }
                    });
                }

                @Override
                public void onDrawerStateChanged(int newState) {
                    if (drawerListener != null)
                        drawerListener.onDrawerStateChanged(newState);
                }
            };
            pulsante.setToolbarNavigationClickListener(toolbarToggleListener);

            drawerLayout.setDrawerListener(pulsante);
            drawerLayout.setMultipaneSupport(false);
        }

        // si procede con gli altri elementi dopo la creazione delle viste
        ComponentTreeObserver vto = dependentDrawer.getComponentTreeObserver();

        vto.addWindowBoundListener(new ComponentTreeObserver.WindowBoundListener() {
            @Override
            public void onWindowBound() {
                // quando l'immagine e' stata caricata

                // change user space to 16:9
                int width = dependentDrawer.getWidth();

                int heightCover = 0;
                switch (drawerHeaderType) {
                    default:
                    case DRAWERHEADER_ACCOUNTS:
                    case DRAWERHEADER_IMAGE:
                    case DRAWERHEADER_CUSTOM:
                        // si fa il rapporto in 16 : 9
                        heightCover = (9 * width) / 16;

                        heightCover += (int) (24 * density);

                        break;
                    case DRAWERHEADER_NO_HEADER:
                        break;
                }

                // adding status bar height for other version of android that not have traslucent status bar
                heightCover += (int) (25 * density);

                //  heightCover (DRAWER HEADER) + 8 (PADDING) + sections + 8 (PADDING) + 1 (DIVISOR) + bottomSections + subheaders
                int heightDrawer = (int) (((8 + 8) * density) + 1 + heightCover + sections.getHeight() + ((density * 48) * bottomSectionList.size()) + (subheaderList.size() * (35 * density)));

                // create the divisor
                Component divisor = new Component(MaterialNavigationDrawer.this);
                ShapeElement shapeElement1 = new ShapeElement();
                shapeElement1.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#8f8f8f")));
                divisor.setBackground(shapeElement1);

                // si aggiungono le bottom sections
                if (heightDrawer >= Utils.getScreenHeight(MaterialNavigationDrawer.this)) {
                    // add the divisor to the section view
                    DirectionalLayout.LayoutConfig paramDivisor = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, 1);
                    paramDivisor.setMargins(0, (int) (8 * density), 0, (int) (8 * density));
                    sections.addComponent(divisor, paramDivisor);


                    for (MaterialSection section : bottomSectionList) {
                        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (48 * density));
                        ComponentParent componentParent = section.getView().getComponentParent();
                        if (componentParent != null) {
                            componentParent.removeComponent(section.getView());
                        }
                        sections.addComponent(section.getView(), params);
                    }
                } else {
                    // add the divisor to the bottom section listview
                    DirectionalLayout.LayoutConfig paramDivisor = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, 1);
                    bottomSections.addComponent(divisor, paramDivisor);

                    for (MaterialSection section : bottomSectionList) {
                        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (48 * density));
                        ComponentParent componentParent = section.getView().getComponentParent();
                        if (componentParent != null) {
                            componentParent.removeComponent(section.getView());
                        }
                        bottomSections.addComponent(section.getView(), params);
                    }
                }
                ComponentTreeObserver obs = dependentDrawer.getComponentTreeObserver();

                // si rimuove il listener
                obs.removeWindowBoundListener(this);
            }

            @Override
            public void onWindowUnbound() {

            }
        });

        MaterialSection section;
        if (true) {

            // init account views
            if (accountManager.size() > 0) {
                currentAccount = accountManager.get(0);
                notifyAccountDataChanged();
            }

            // init section
            if (defaultSectionLoaded < 0 || defaultSectionLoaded >= sectionList.size()) {
                throw new RuntimeException("You are trying to open at startup a section that does not exist");
            }

            section = sectionList.get(defaultSectionLoaded);
            if (section.getTarget() != MaterialSection.TARGET_FRAGMENT)
                throw new RuntimeException("The first section added must have a fragment as target");
        } else {

        }
        currentSection = section;
        section.select();
        setFragment((Fraction) section.getTargetFragment(), section.getTitle(), null, false);
//
//        // change the toolbar color for the first section
        changeToolbarColor(section);
//
//        // add the first section to the child stack
        childFragmentStack.add((Fraction) section.getTargetFragment());
        childTitleStack.add(section.getTitle());
//
//        // learning pattern
        if (learningPattern) {
            drawerLayout.openSmoothly();
            disableLearningPattern();
        }
    }


    private Component.ClickedListener toolbarToggleListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component v) {
            if (isCurrentFragmentChild) {
                onBackPressed();
            }
        }
    };

    private void addButtonView() {
        bottomSections.removeAllComponents();
        DirectionalLayout.LayoutConfig paramDivisor = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, 1);
        bottomSections.addComponent(addDivisor(), paramDivisor);

        for (MaterialSection section : bottomSectionList) {
            DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, (int) (48 * density));
            bottomSections.addComponent(section.getView(), params);
        }
    }

    private Component addDivisor() {
        // create the divisor
        Component divisor = new Component(MaterialNavigationDrawer.this);
        ShapeElement shapeElement1 = new ShapeElement(MaterialNavigationDrawer.this, ResourceTable.Graphic_background_divisor);
        divisor.setBackground(shapeElement1);
        return divisor;
    }


    @Override
    public void setUIContent(ComponentContainer componentContainer) {
        throw new RuntimeException("The library have it's own content, please move all content inside section's fragments");
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.getSlideState() == SlideDrawer.DrawerState.OPEN) {
            drawerLayout.closeSmoothly();
            return;
        }
        if (!isCurrentFragmentChild) {
            switch (backPattern) {
                default:
                case BACKPATTERN_BACK_ANYWHERE:
                    super.onBackPressed();
                    break;
                case BACKPATTERN_BACK_TO_FIRST:
                    MaterialSection section = sectionList.get(0);
                    if (currentSection == section)
                        super.onBackPressed();
                    else {
                        section.select();
                        changeToolbarColor(section);
                        setFragment((Fraction) section.getTargetFragment(), section.getTitle(), (Fraction) currentSection.getTargetFragment());
                        afterFragmentSetted((Fraction) section.getTargetFragment(), section.getTitle());
                        syncSectionsState(section);
                    }
                    break;
                case BACKPATTERN_CUSTOM:
                    MaterialSection backedSection = backToSection(getCurrentSection());

                    if (currentSection == backedSection)
                        super.onBackPressed();
                    else {
                        if (backedSection.getTarget() != MaterialSection.TARGET_FRAGMENT) {
                            throw new RuntimeException("The restored section must have a fragment as target");
                        }
                        backedSection.select();
                        changeToolbarColor(backedSection);

                        setFragment((Fraction) backedSection.getTargetFragment(), backedSection.getTitle(), (Fraction) currentSection.getTargetFragment());
                        afterFragmentSetted((Fraction) backedSection.getTargetFragment(), backedSection.getTitle());
                        syncSectionsState(backedSection);
                    }
                    break;
            }
        } else {
            if (childFragmentStack.size() <= 1) {
                isCurrentFragmentChild = false;
                pulsante.setCurrentFragmentChild(isCurrentFragmentChild);
                onBackPressed();
            } else {
                // reload the child before
                Fraction newFragment = childFragmentStack.get(childFragmentStack.size() - 2);
                String newTitle = childTitleStack.get(childTitleStack.size() - 2);

                // get and remove the last child
                Fraction currentFragment = childFragmentStack.remove(childFragmentStack.size() - 1);
                childTitleStack.remove(childTitleStack.size() - 1);


                setFragment(newFragment, newTitle, currentFragment);

                if (childFragmentStack.size() == 1) {
                    // user comed back to master section
                    isCurrentFragmentChild = false;
                    pulsante.setCurrentFragmentChild(isCurrentFragmentChild);
                }
            }
        }

    }

    public void setFragmentChild(Fraction fragment, String title) {
        isCurrentFragmentChild = true;
        pulsante.setCurrentFragmentChild(isCurrentFragmentChild);

        // replace the fragment
        setFragment(fragment, title, childFragmentStack.get(childFragmentStack.size() - 1));

        // add to the stack the child
        childFragmentStack.add(fragment);
        childTitleStack.add(title);
    }

    private void setDrawerTouchable(boolean isTouchable) {
        drawerTouchLocked = !isTouchable;

        for (MaterialSection section : sectionList) {
            section.setTouchable(isTouchable);
        }
        for (MaterialSection section : bottomSectionList) {
            section.setTouchable(isTouchable);
        }
    }

    /**
     * Method used with BACKPATTERN_CUSTOM to retrieve the section which is restored
     *
     * @param currentSection the section used at this time
     * @return the Section to restore that has Fragment as target (or currentSection for exit from activity)
     */
    protected MaterialSection backToSection(MaterialSection currentSection) {
        return currentSection;
    }

    /**
     * Set the section informations.<br />
     * In short:
     * <ul>
     *     <li>set the section title into the toolbar</li>
     *     <li>set the section color to the toolbar</li>
     *     <li>open/call the target</li>
     * </ul>
     * <p>
     * This method is equal to a user tap on a drawer section.
     *
     * @param section the section which is replaced
     */
    public void setSection(MaterialSection section) {
        section.select();
        syncSectionsState(section);

        switch (section.getTarget()) {
            case MaterialSection.TARGET_FRAGMENT:
                // se l'utente clicca sulla stessa schermata in cui si trova si chiude il drawer e basta
                if (section == currentSection) {
                    if (!deviceSupportMultiPane())
                        drawerLayout.closeSmoothly();
                    return;
                }
                changeToolbarColor(section);
                setFragment((Fraction) section.getTargetFragment(), section.getTitle(), (Fraction) currentSection.getTargetFragment());
                afterFragmentSetted((Fraction) section.getTargetFragment(), section.getTitle());
                break;
            case MaterialSection.TARGET_ACTIVITY:
                this.startAbility(section.getTargetIntent());
                if (!deviceSupportMultiPane()) {
                    drawerLayout.closeSmoothly();
                }
                break;
            case MaterialSection.TARGET_LISTENER:
                // call the section listener
                section.getTargetListener().onClick(section);
                if (!deviceSupportMultiPane()) {
                    drawerLayout.closeSmoothly();
                }
            default:
                break;
        }

        // se il target e' un activity la sezione corrente rimane quella precedente
        if (section.getTarget() != MaterialSection.TARGET_ACTIVITY) {
            syncSectionsState(section);
        }
    }

    private void switchAccounts(final MaterialAccount newAccount) {
        // for minor version no animation is used.
        // switch numbers
        currentAccount.setAccountNumber(newAccount.getAccountNumber());
        newAccount.setAccountNumber(MaterialAccount.FIRST_ACCOUNT);
        // change pointer to newAccount
        currentAccount = newAccount;
        // refresh views
        notifyAccountDataChanged();

        if (!deviceSupportMultiPane()) {
            drawerLayout.closeSmoothly();
        }
    }


    @Override
    public void onUserPhotoLoaded(MaterialAccount account) {
        if (account.getAccountNumber() <= MaterialAccount.THIRD_ACCOUNT)
            notifyAccountDataChanged();
    }

    @Override
    public void onBackgroundLoaded(MaterialAccount account) {
        if (account.getAccountNumber() <= MaterialAccount.THIRD_ACCOUNT)
            notifyAccountDataChanged();
    }

    // method used for change supports
    public void setAccountListener(MaterialAccountListener listener) {
        this.accountListener = listener;
    }

    public void disableLearningPattern() {
        learningPattern = false;
    }

    private void setFragment(Fraction fragment, String title, Fraction oldFragment) {
        setFragment(fragment, title, oldFragment, false);
    }

    private void setFragment(Fraction fragment, String title, Fraction oldFragment, boolean hasSavedInstanceState) {
        // si setta il titolo
        setTitle(title);
        FractionScheduler ft = getFractionManager().startFractionScheduler();

        if (oldFragment != null && oldFragment != fragment) {
            ft.remove((ohos.aafwk.ability.fraction.Fraction) oldFragment);
        }


        if (!hasSavedInstanceState) {// se non e' avvenuta una rotazione
            ft.replace(ResourceTable.Id_frame_container, (ohos.aafwk.ability.fraction.Fraction) fragment).submit();
        }
    }

    private void afterFragmentSetted(Fraction fragment, String title) {
        // remove the last child from the stack
        if (!isCurrentFragmentChild) {
            childFragmentStack.remove(childFragmentStack.size() - 1);
            childTitleStack.remove(childTitleStack.size() - 1);
        } else
            for (int i = childFragmentStack.size() - 1; i >= 0; i--) { // if a section is clicked when user is into a child remove all childs from stack
                childFragmentStack.remove(i);
                childTitleStack.remove(i);
            }

        // add to the childStack the Fragment and title
        childFragmentStack.add(fragment);
        childTitleStack.add(title);
        isCurrentFragmentChild = false;
        pulsante.setCurrentFragmentChild(isCurrentFragmentChild);
        if (!deviceSupportMultiPane()) {
        } else {
        }
    }

    public void setTitle(CharSequence title) {
        getToolbar().setTitle(title);
    }

    public void changeToolbarIcon(Toolbar.IconType type) {
        if (toolbar != null) {
            if (type == Toolbar.IconType.iconBack) {
                toolbar.chengeIconColor(ResourceTable.Media_back_def);
            } else {
                toolbar.chengeIconColor(ResourceTable.Media_horizontal_navigation_icon);
            }

        }
    }

    public void changeToolbarColor(MaterialSection section) {

        int sectionPrimaryColor;
        int sectionPrimaryColorDark;
        if (section.hasSectionColor() && !uniqueToolbarColor) {
            if (!section.hasSectionColorDark())
                sectionPrimaryColorDark = darkenColor(section.getSectionColor());
            else
                sectionPrimaryColorDark = section.getSectionColorDark();

            sectionPrimaryColor = section.getSectionColor();
        } else {
            sectionPrimaryColorDark = primaryDarkColor;
            sectionPrimaryColor = primaryColor;
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(sectionPrimaryColor));
        this.getToolbar().setBackground(shapeElement);
        ShapeElement ColorDrawable = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(sectionPrimaryColorDark));
        statusBar.setImageElement(ColorDrawable);

    }

    public void changeToolbarColor(int primaryColor, int primaryDarkColor) {
        if (statusBar != null) {
            ShapeElement ColorDrawable = new ShapeElement();
            ColorDrawable.setRgbColor(RgbColor.fromArgbInt(primaryDarkColor));
            statusBar.setImageElement(ColorDrawable);
        }


        if (getToolbar() != null) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(primaryColor));
            this.getToolbar().setBackground(shapeElement);
        }

    }

    public void setBackPattern(int backPattern) {
        this.backPattern = backPattern;
    }


    public void setDrawerHeaderCustom(Component view) {
        if (drawerHeaderType != DRAWERHEADER_CUSTOM)
            throw new RuntimeException("Your header is not setted to Custom, check in your styles.xml");

        ComponentContainer.LayoutConfig params = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        customDrawerHeader.addComponent(view, params);
    }

    protected int darkenColor(int color) {
        if (color == primaryColor)
            return primaryDarkColor;
        return color;
    }


    public void setDrawerHeaderImage(PixelMap backgroundId) {
        switch (drawerHeaderType) {
            case DRAWERHEADER_IMAGE:
                Image image = new Image(this);
                DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
                image.setScaleMode(Image.ScaleMode.ZOOM_CENTER);
                image.setPixelMap(backgroundId);

                customDrawerHeader.addComponent(image, params);
                break;
            case DRAWERHEADER_ACCOUNTS:
                usercover.setPixelMap(backgroundId);
                break;
            default:
                throw new RuntimeException("Your drawer configuration don't support a background image, check in your styles.xml");
        }
    }

    public void setUsername(String name) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }
        username.setText(name);
    }

    // Method used for customize layout

    public void setUserEmail(String email) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        usermail.setText(email);
    }

    public void addSection(MaterialSection section) {
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, (int) (48 * density));
        section.setTypeface(fontManager.getRobotoMedium(this));
        sectionList.add(section);
        sections.addComponent(section.getView(), params);

        // add the element to the list
        elementsList.add(new Element(Element.TYPE_SECTION, section));
    }

    public void addAccountSection(MaterialSection section) {
        section.setTypeface(fontManager.getRobotoMedium(this));
        accountSectionList.add(section);
    }

    public void addAccount(MaterialAccount account) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        account.setAccountListener(this);
        account.setAccountNumber(accountManager.size());
        accountManager.add(account);
    }

    /**
     * Reload Application data from Account Information
     */
    public void notifyAccountDataChanged() {
        if (accountManager == null || accountManager.isEmpty()) {
            return;
        }
        switch (accountManager.size()) {
            default:
            case 3:
                this.setThirdAccountPhoto(findAccountNumber(MaterialAccount.THIRD_ACCOUNT).getCircularPhoto());
            case 2:
                this.setSecondAccountPhoto(findAccountNumber(MaterialAccount.SECOND_ACCOUNT).getCircularPhoto());
            case 1:
                this.setFirstAccountPhoto(currentAccount.getCircularPhoto());
                this.setDrawerHeaderImage(currentAccount.getBackground());
                this.setUsername(currentAccount.getTitle());
                this.setUserEmail(currentAccount.getSubTitle());
            case 0:
        }
    }

    public void setSecondAccountPhoto(PixelMap photo) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        if (userSecondPhoto instanceof RoundImage) {
            ((RoundImage) userSecondPhoto).setPixelMapAndCircle(photo);
        } else {
            userSecondPhoto.setPixelMap(photo);
        }

    }

    public void setFirstAccountPhoto(PixelMap photo) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }
        if (userphoto instanceof RoundImage) {
            ((RoundImage) userphoto).setPixelMapAndCircle(photo);
        } else {
            userphoto.setPixelMap(photo);
        }

    }

    /**
     * Set the section that will be opened when the activity starts.
     * NOTE: 0 is for the first section
     *
     * @param sectionNumber is the number of section that you have added in the init() method.
     */
    public void setDefaultSectionLoaded(int sectionNumber) {
        defaultSectionLoaded = sectionNumber;
    }

    // create sections

    public MaterialSection newSection(String title, int icon, Fraction target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, int icon, Fraction target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, int icon, Intent target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, int icon, Intent target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, int icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, int icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, PixelMap icon, Fraction target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, PixelMap icon, Fraction target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, PixelMap icon, Intent target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, PixelMap icon, Intent target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, PixelMap icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, PixelMap icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    // private methods
    private MaterialAccount findAccountNumber(int number) {
        for (MaterialAccount account : accountManager)
            if (account.getAccountNumber() == number)
                return account;


        return null;
    }

    public void setThirdAccountPhoto(PixelMap photo) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }
        if (userThirdPhoto instanceof RoundImage) {
            ((RoundImage) userThirdPhoto).setPixelMapAndCircle(photo);
        } else {
            userThirdPhoto.setPixelMap(photo);
        }

    }

    private boolean deviceSupportMultiPane() {
        return false;
    }

    // get methods

    public Toolbar getToolbar() {
        return toolbar;
    }

    public MaterialSection newSection(String title, Fraction target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_NO_ICON, rippleSupport, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    @SuppressWarnings("unchecked")
    public MaterialSection newSection(String title, Intent target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_NO_ICON, rippleSupport, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    @SuppressWarnings("unchecked")
    public MaterialSection newSection(String title, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fraction>(this, MaterialSection.ICON_NO_ICON, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    /**
     * Get the section which the user see
     *
     * @return the current section
     */
    public MaterialSection getCurrentSection() {
        return currentSection;
    }


    public void addBottomSection(MaterialSection section) {
        section.setTypeface(fontManager.getRobotoRegular(this));
        bottomSectionList.add(section);

        addButtonView();

        // add the element to the list
        elementsList.add(new Element(Element.TYPE_BOTTOM_SECTION, section));
    }

    // abstract methods

    public abstract void init(IntentParams savedInstanceState);

    @Override
    protected void onActive() {
        super.onActive();
        notifyAccountDataChanged();
//        addButtonView();
    }
}
