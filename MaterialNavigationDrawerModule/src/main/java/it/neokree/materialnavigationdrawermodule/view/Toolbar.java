/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.neokree.materialnavigationdrawermodule.view;

import it.neokree.materialnavigationdrawermodule.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * Toolbar
 *
 * @since 2021-05-10
 */
public class Toolbar extends DirectionalLayout {
    private Text mIdTitle;
    private ToolbarListener mToolbarListener;

    private Image mIdIcon;

    /**
     * IconType
     *
     * @since 2021-05-10
     */
    public enum IconType {
        iconMenu, iconBack
    }

    /**
     * Toolbar
     *
     * @param context
     */
    public Toolbar(Context context) {
        this(context, null);
    }

    /**
     * Toolbar
     *
     * @param context
     * @param attrSet
     */
    public Toolbar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * Toolbar
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public Toolbar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {
        Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_toolbar, null, false);
        Component idIcon = parse.findComponentById(ResourceTable.Id_icon);
        idIcon.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mToolbarListener != null) {
                    mToolbarListener.onClickIcon();
                }
            }
        });
        mIdTitle = (Text) parse.findComponentById(ResourceTable.Id_title);
        addComponent(parse);
        mIdIcon = (Image) findComponentById(ResourceTable.Id_icon);
    }

    /**
     * setTitle
     *
     * @param charSequence
     */
    public void setTitle(CharSequence charSequence) {
        if (mIdTitle != null) {
            mIdTitle.setText(charSequence.toString());
        }
    }

    public void chengeIconColor(int resId) {
        mIdIcon.setBackground(null);
        mIdIcon.setPixelMap(resId);
    }

    public void setToolbarListener(ToolbarListener toolbarListener) {
        mToolbarListener = toolbarListener;
    }

    /**
     * ToolbarListener
     *
     * @since 2021-05-10
     */
    public interface ToolbarListener {
        /**
         * onClickIcon
         */
        void onClickIcon();
    }
}
