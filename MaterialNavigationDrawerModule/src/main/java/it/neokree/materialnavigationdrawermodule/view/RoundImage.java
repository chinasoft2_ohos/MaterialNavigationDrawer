/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.neokree.materialnavigationdrawermodule.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;

import java.io.InputStream;

/**
 * RoundImage
 *
 * @since 2021-04-19
 */
public class RoundImage extends Image implements Component.TouchEventListener {
    private PixelMapHolder pixelMapHolder;
    private RectFloat rectDst;
    private RectFloat rectSrc;

    /**
     * RoundImage
     *
     * @param context
     */
    public RoundImage(Context context) {
        this(context, null);
    }

    /**
     * RoundImage
     *
     * @param context
     * @param attrSet
     */
    public RoundImage(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
        setTouchEventListener(this);
    }

    /**
     * 加载包含该控件的xml布局，会执行该构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public RoundImage(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private void onRoundRectDraw(int radius) {
        this.addDrawTask((view, canvas) -> {
            if (pixelMapHolder == null) {
                return;
            }
            synchronized (pixelMapHolder) {
                rectDst = new RectFloat(0, 0, getWidth(), getHeight());
                canvas.drawPixelMapHolderRoundRectShape(pixelMapHolder, rectSrc, rectDst, radius, radius);
                pixelMapHolder = null;
            }
        });
    }

    private void onCircleDraw() {
        this.addDrawTask((view, canvas) -> {
//            if (pixelMapHolder == null) {
//                return;
//            }
            synchronized (pixelMapHolder) {
                rectDst = new RectFloat(0, 0, getWidth(), getHeight());
                canvas.drawPixelMapHolderRoundRectShape(pixelMapHolder, rectSrc,
                    rectDst, getWidth() / 2, getHeight() / 2);
//                pixelMapHolder = null;
            }
        });
    }

    /**
     * 获取原有Image中的位图资源后重新检验绘制该组件
     *
     * @param pixelMap
     */
    private void putPixelMap(PixelMap pixelMap) {
        if (pixelMap != null) {
            rectSrc = new RectFloat(0, 0, pixelMap.getImageInfo().size.width, pixelMap.getImageInfo().size.height);
            pixelMapHolder = new PixelMapHolder(pixelMap);
            invalidate();
        } else {
            pixelMapHolder = null;
            setPixelMap(null);
        }
    }

    /**
     * 通过资源ID获取位图对象
     *
     * @param resId resId
     * @return return
     */
    private PixelMap getPixelMap(int resId) {
        InputStream drawableInputStream = null;
        try {
            drawableInputStream = getResourceManager().getResource(resId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(drawableInputStream, null);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (drawableInputStream != null) {
                    drawableInputStream.close();
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        return null;
    }

    /**
     * 对外调用的api，设置圆形图片方法
     *
     * @param resId
     */
    public void setPixelMapAndCircle(int resId) {
        PixelMap pixelMap = getPixelMap(resId);
        putPixelMap(pixelMap);
        onCircleDraw();
    }

    /**
     * 对外调用的api，设置圆形图片方法
     *
     * @param pixelMap
     */
    public void setPixelMapAndCircle(PixelMap pixelMap) {
        putPixelMap(pixelMap);
        onCircleDraw();
    }

    /**
     * 对外调用的api，设置圆角图片方法
     *
     * @param resId
     * @param radius
     */
    public void setPixelMapAndRoundRect(int resId, int radius) {
        PixelMap pixelMap = getPixelMap(resId);
        putPixelMap(pixelMap);
        onRoundRectDraw(radius);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return false;
    }
}
