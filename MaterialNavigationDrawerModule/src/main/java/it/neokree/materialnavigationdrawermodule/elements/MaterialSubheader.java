
package it.neokree.materialnavigationdrawermodule.elements;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import it.neokree.materialnavigationdrawermodule.util.Utils;

/**
 * lpq
 *
 * @since 2021-05-08
 */
public class MaterialSubheader {
    private CharSequence title;
    private int titleColor;

    private Text text;
    private Component view;

    public MaterialSubheader(Context ctx) {

        float density = (float) ctx.getResourceManager().getDeviceCapability().screenDensity / (float) 160;

        // create layout
        DirectionalLayout layout = new DirectionalLayout(ctx);
        layout.setOrientation(DirectionalLayout.VERTICAL);

        // inflate the line
        Component view = new Component(ctx);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(ohos.agp.utils.Color.getIntColor("#8f8f8f")));
        view.setBackground(shapeElement);
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, 1);
        params.setMargins(0, (int) (8 * density), 0, (int) (8 * density));
        layout.addComponent(view, params);

        // inflate the text
        text = new Text(ctx);
        Utils.setAlpha(text, 0.54f);
        text.setTextSize(14, Text.TextSizeType.FP);
        text.setTextAlignment(TextAlignment.START);
        DirectionalLayout.LayoutConfig paramsText = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        paramsText.setMargins((int) (16 * density), 0, (int) (16 * density), (int) (4 * density));

        layout.addComponent(text, paramsText);
        this.view = layout;

    }

    public void setTitle(CharSequence title) {
        this.title = title;
        text.setText(title.toString());
    }

    public void setTitleColor(int color) {
        titleColor = color;
        text.setTextColor(new Color(color));
    }

    public int getTitleColor() {
        return titleColor;
    }

    public CharSequence getTitle() {
        return title;
    }

    public Component getView() {
        return view;
    }
}
