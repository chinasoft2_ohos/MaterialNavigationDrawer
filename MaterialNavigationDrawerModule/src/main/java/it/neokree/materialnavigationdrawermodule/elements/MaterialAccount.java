
package it.neokree.materialnavigationdrawermodule.elements;

import ohos.agp.components.Component;
import ohos.agp.text.Font;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.media.image.PixelMap;


import it.neokree.materialnavigationdrawermodule.elements.listeners.MaterialSectionListener;
import it.neokree.materialnavigationdrawermodule.util.Utils;

/**
 * Created by neokree on 11/12/14.
 */
public class MaterialAccount {

    // datas
    private PixelMap photo;
    private PixelMap background;
    private PixelMap circularPhoto;
    private String title;
    private String subTitle;
    private int accountNumber;
    private String notifications;

    private boolean hasNotifications;

    private ResourceManager resources;
    private OnAccountDataLoaded listener;
    private MaterialSection sectionView;

    public static final int FIRST_ACCOUNT = 0;
    public static final int SECOND_ACCOUNT = 1;
    public static final int THIRD_ACCOUNT = 2;

    public MaterialAccount(ResourceManager resources, String title, String subTitle, int photo, PixelMap background) {
        this.title = title;
        this.subTitle = subTitle;
        this.resources = resources;

        // resize and caching bitmap
        new ResizePhotoResource().execute(photo);
        if (background != null)
            new ResizeBackgroundBitmap().execute(background);

    }

    public MaterialAccount(ResourceManager resources, String title, String subTitle, int photo, int background) {
        this.title = title;
        this.subTitle = subTitle;
        this.resources = resources;

        // resize and caching bitmap
        new ResizePhotoResource().execute(photo);
        new ResizeBackgroundResource().execute(background);
    }

    public MaterialAccount(ResourceManager resources, String title, String subTitle, PixelMap photo, int background) {
        this.title = title;
        this.subTitle = subTitle;
        this.resources = resources;

        // resize and caching bitmap
        if (photo != null)
            new ResizePhotoBitmap().execute(photo);
        new ResizeBackgroundResource().execute(background);
    }

    public MaterialAccount(ResourceManager resources, String title, String subTitle, PixelMap photo, PixelMap background) {
        this.title = title;
        this.subTitle = subTitle;
        this.resources = resources;

        // resize and caching bitmap
        if (photo != null)
            new ResizePhotoBitmap().execute(photo);
        if (background != null)
            new ResizeBackgroundBitmap().execute(background);
    }

    // setter

    public void setPhoto(int photo) {
        new ResizePhotoResource().execute(photo);
    }

    public void setPhoto(PixelMap photo) {
        new ResizePhotoBitmap().execute(photo);
    }

    public void setBackground(PixelMap background) {
        new ResizeBackgroundBitmap().execute(background);
    }

    public void setBackground(int background) {
        new ResizeBackgroundResource().execute(background);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public void setAccountNumber(int number) {
        this.accountNumber = number;
    }

    public void setAccountListener(OnAccountDataLoaded listener) {
        this.listener = listener;
    }

    public MaterialAccount setNotifications(int number) {
        hasNotifications = true;
        notifications = String.valueOf(number);

        if (number >= 100) {
            notifications = "99+";
        }
        if (number < 0) {
            notifications = "0";
        }

        return this;

    }

    // getter

    public PixelMap getPhoto() {
        return photo;
    }

    public PixelMap getBackground() {
        return background;
    }

    public PixelMap getCircularPhoto() {
        return circularPhoto;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public Component getSectionView(Context ctx, Font font, MaterialSectionListener listener, boolean rippleSupport, int position) {
        if (sectionView == null) {
            sectionView = new MaterialSection(ctx, MaterialSection.ICON_40DP, rippleSupport, MaterialSection.TARGET_LISTENER);
            sectionView.useRealColor();
        }

        // set dei dati passati
        sectionView.setTypeface(font);
        sectionView.setOnClickListener(listener);

        // set dei dati dell'account
        sectionView.setIcon(getCircularPhoto());
        sectionView.setTitle(getTitle());
        if (hasNotifications) {
            sectionView.setNotificationsText(notifications);
        }
        sectionView.setAccountPosition(position);

        return sectionView.getView();
    }


    public interface OnAccountDataLoaded {

        void onUserPhotoLoaded(MaterialAccount account);

        void onBackgroundLoaded(MaterialAccount account);
    }

    // asynctasks

    private class ResizePhotoResource {


        protected void execute(Integer... params) {
            Point photoSize = Utils.getUserPhotoSize(resources);

            PixelMap photo = Utils.resizeBitmapFromResource(resources, params[0], photoSize.getPointXToInt(), photoSize.getPointYToInt());

            circularPhoto = photo;
            onPostExecute(photo);
        }


        protected void onPostExecute(PixelMap drawable) {
            photo = drawable;

            if (listener != null)
                listener.onUserPhotoLoaded(MaterialAccount.this);
        }
    }

    private class ResizePhotoBitmap {


        protected void execute(PixelMap... params) {
            Point photoSize = Utils.getUserPhotoSize(resources);


            PixelMap photo = Utils.resizeBitmap(params[0], photoSize.getPointXToInt(), photoSize.getPointYToInt());
            circularPhoto = photo;
            onPostExecute(photo);
        }


        protected void onPostExecute(PixelMap drawable) {
            photo = drawable;

            if (listener != null)
                listener.onUserPhotoLoaded(MaterialAccount.this);
        }
    }

    private class ResizeBackgroundResource {

        protected void execute(Integer... params) {
            Point backSize = Utils.getBackgroundSize(resources);

            PixelMap back = Utils.resizeBitmapFromResource(resources, params[0], backSize.getPointXToInt(), backSize.getPointYToInt());

            onPostExecute(back);
        }


        protected void onPostExecute(PixelMap drawable) {
            background = drawable;

            if (listener != null)
                listener.onBackgroundLoaded(MaterialAccount.this);
        }
    }

    private class ResizeBackgroundBitmap {


        protected void execute(PixelMap... params) {
            Point backSize = Utils.getBackgroundSize(resources);

            PixelMap back = Utils.resizeBitmap(params[0], backSize.getPointXToInt(), backSize.getPointYToInt());

            onPostExecute(back);
        }

        protected void onPostExecute(PixelMap drawable) {
            background = drawable;

            if (listener != null)
                listener.onBackgroundLoaded(MaterialAccount.this);
        }
    }
}
