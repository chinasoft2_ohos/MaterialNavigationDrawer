package it.neokree.materialnavigationdrawermodule.elements.listeners;

import it.neokree.materialnavigationdrawermodule.elements.MaterialAccount;

/**
 * lpq
 *
 * @since 2021-04-19
 */
public interface MaterialAccountListener {
    /**
     * onAccountOpening
     *
     * @param account
     */
    void onAccountOpening(MaterialAccount account);

    /**
     * onChangeAccount
     *
     * @param newAccount
     */
    void onChangeAccount(MaterialAccount newAccount);
}
