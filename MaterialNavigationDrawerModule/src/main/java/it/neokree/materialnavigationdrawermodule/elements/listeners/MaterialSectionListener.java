package it.neokree.materialnavigationdrawermodule.elements.listeners;


import it.neokree.materialnavigationdrawermodule.elements.MaterialSection;

/**
 * lipeiquan
 *
 * @since 2021-05-10
 */
public interface MaterialSectionListener {

    /**
     * onClick
     *
     * @param section
     */
    void onClick(MaterialSection section);
}
