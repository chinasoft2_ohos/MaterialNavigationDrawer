/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.neokree.materialnavigationdrawermodule.constant;

/**
 * Constant
 *
 * @since 2021-05-13
 */
public class Constant {
    /**
     * MaterialNavigationDrawer
     *
     * @since 2021-05-13
     */
    public static final class MaterialNavigationDrawer {
        /**
         * KEY
         */
        public static final String KEY = "material_navigation_drawer";
        /**
         * RIPPLE_BACKPORT
         */
        public static final String RIPPLE_BACKPORT = "ripple_backport";
        /**
         * DRAWER_TYPE
         */
        public static final String DRAWER_TYPE = "drawer_type";
        /**
         * UNIQUE_TOOLBAR_COLOR
         */
        public static final String UNIQUE_TOOLBAR_COLOR = "unique_toolbar_color";
        /**
         * SINGLE_ACCOUNT
         */
        public static final String SINGLE_ACCOUNT = "single_account";
        /**
         * MULTIPANE_SUPPORT
         */
        public static final String MULTIPANE_SUPPORT = "multipane_support";
        /**
         * DRAWER_COLOR
         */
        public static final String DRAWER_COLOR = "drawer_color";
        /**
         * SECTION_STYLE
         */
        public static final String SECTION_STYLE = "section_style";
        /**
         * SUBHEADER_STYLE
         */
        public static final String SUBHEADER_STYLE = "subheader_style";
        /**
         * ACCOUNT_STYLE
         */
        public static final String ACCOUNT_STYLE = "account_style";
        /**
         * LEARNING_PATTERN
         */
        public static final String LEARNING_PATTERN = "learning_pattern";
        /**
         * DEFAULT_SECTION_LOADED
         */
        public static final String DEFAULT_SECTION_LOADED = "default_section_loaded";
        /**
         * TOOLBAR_ELEVATION
         */
        public static final String TOOLBAR_ELEVATION = "toolbar_elevation";
    }
}
